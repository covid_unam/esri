echo Batch file Add arguments to R eventually

:: RMDIR /S /Q "repounam_2020-07-23"

"C:\Program Files\R\R-4.2.2\bin\R.exe" CMD BATCH "source.R"
:: set url="https://gitlab.com/covid_unam/esri/-/raw/master/source.R"
:: "C:\Program Files\R\R-4.2.2\bin\R.exe" CMD BATCH call "https://gitlab.com/covid_unam/esri/-/raw/master/source.R"
:: "C:\Program Files\R\R-4.2.2\bin\R.exe" CMD BATCH "--args argument1 argument2 argument3" Rcode.R Rcodeoutput.txt

del ".RData" /s /f /q
del ".Rhistory" /s /f /q
del "*.zip" /f /q

echo Done!

exit